import pytest
from mymain.code import *

def test_add():
    assert add(3,1)==4

# Assert that a certain exception is raised
def test_myexit():
    with pytest.raises(SystemExit):
        myexit()

# Dependency Injection
@pytest.fixture(scope="module")
def mylist(): return [4,2,5]


def test_sum_list(mylist):
    assert sum_list(mylist) == 11


# Multiple Test Cases
@pytest.mark.parametrize("num1, num2, num3",
                         [(1,3,4),
                          (2,5,7),
                         ]
                         )
def test_add2(num1, num2, num3):
    assert add(num1, num2) == num3


def test_slow_function(mocker):
    mocker.patch("mymain.code.pd.read_csv", return_value=6)
    assert slow_function()==6

from unittest.mock import patch
@patch("mymain.code.pd.read_csv")
def test_slow_function_v2(read_csv):
    read_csv.return_value = 7
    assert slow_function()==7