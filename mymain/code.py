import pandas as pd

def add(x, y):
    return x + y

def myexit():
    raise SystemExit(1)

def sum_list(iterable):
    result = 0
    for value in iterable:
        result += value
    return result

def slow_function():
    db_result = pd.read_csv("super_huge_file.csv")
    # do something here
    return db_result


if __name__ == "__main__":
    print(add(4,2))
    myexit()