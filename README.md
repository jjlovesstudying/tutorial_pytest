# Pytest Tutorial
### Setting Up in Pycharm
```
File -> Settings -> Tools -> Python Integrated Tools
Testing -> pytest
```
Note: 
- For your python test code, please ensure format `test_*.py` or `*_test.py`.
- Please start all function name with `test_xxx`.

### File Hierarchy
```
project
|--- mymain
     |--- __init__.py
     |--- code.py

|--- tests
     |--- .pytest_cache <- do not commit this folder
     |--- .gitignore
     |--- __init__.py
     |--- test_code.py

```

### Assert Exception
```
def myexit():
    raise SystemExit(1)

def test_myexit():
    with pytest.raises(SystemExit):
        myexit()
```

### Fixtures
Dependency Injection
```
def sum_list(iterable):
    result = 0
    for val in iterable:
        result += val
    return result

@pytest.fixture(scope="module")
def mylist():
    return [4,2,5]

def test_sum_list(mylist):
    assert sum_list(mylist) == 11
```

Valid values of **scope**: `function`, `class`, `module`, `package`, `session`

By default, executes all fixtures once per function -> Can be expensive (e.g. database connection).
Scope will allow reuse the instance between test

### Parameterize
Run multiple test cases
```
@pytest.mark.parameterize("num1", "num2", "num3",
                          [ (1,3,4), (2,5,7) ] )
def test_add(num1, num2, num3):
    assert num1 + num2 == num3
```

### Mock
Simulate behaviour of real objects
```
# Assume this function to be in mymain/code.py
def slow_fn():
    db_result = pd.read_csv()
    # do something here
    return db_result

# Method 1
# Note: Name will be the fully qualified name + function name
def test_slow_fn(mocker):
    mocker.patch("mymain.code.pd.read_csv", return_value=6)
    assert slow_fn == 6

# Method 2
from unittest.mock import patch
@patch("mymain.code.pd.read_csv")
def test_slow_fn_v2(read_csv):
    read_csv.return_value = 7
    assert slow_fn() == 7
```